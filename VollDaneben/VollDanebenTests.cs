﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace VollDaneben
{
    public class VollDanebenTests
    {

        [Fact]
        private void AcceptanceTest_CostFunction()
        {
            var luckyNumbers = new[] { 1, 15, 100, 200, 300 };
            var dealersNumbers = new[] { 1, 35, 117, 321, 448, 500, 678, 780, 802, 999 };

            Assert.Equal(14 + 17 + 83 + 21, Casino.CalculateTotalPriceMoney(luckyNumbers, dealersNumbers));
        }

        [Fact]
        private void Test_MinimumDistanceForOneNumber()
        {
            var dealersNumbers = new[] { 1, 35, 117, 321, 448, 500, 678, 780, 802, 999 };
            var luckyNumber = 15;
            Assert.Equal(14, actual: luckyNumber.MinimumDistanceToDealersNumber(dealersNumbers));
        }

        [Theory]
        [InlineData(15, 0)]
        [InlineData(83, 2)]
        [InlineData(300, 3)]
        private void Test_IndexOfClosestNumber(int luckyNumber, int expectedIndex)
        {
            var dealersNumbers = new[] { 1, 35, 117, 321, 448, 500, 678, 780, 802, 999 };
            Assert.Equal(expectedIndex, actual: luckyNumber.FindClosestNumber(dealersNumbers));
        }

        private void TestRandomGenerator()
        {
            var dealersNumbers = Casino.GenerateDealersNumbers(10);
            Assert.Equal(10, dealersNumbers.Length);
            Assert.True(dealersNumbers.Max() <= 1000);
            Assert.True(dealersNumbers.Min() >= 1);
        }

        [Fact]
        private void AcceptanceTest_MustBeZeroCost()
        {
            var luckyNumbers = new[] { 1, 15, 100, 200, 300 };
            var dealersNumbers = Casino.FindBestDealerNumbers(luckyNumbers);

            Assert.Equal(0, Casino.CalculateTotalPriceMoney(luckyNumbers, dealersNumbers));
        }

        [Fact]
        private void AcceptanceTest_Arbitrary()
        {
            var luckyNumbers = new[] { 1, 15, 100, 200, 300, 66, 400, 237, 1000, 3 };
            var dealersNumbers = Casino.FindBestDealerNumbers(luckyNumbers);

            Assert.Equal(0, Casino.CalculateTotalPriceMoney(luckyNumbers, dealersNumbers));
        }

        [Fact]
        private void AcceptanceTest_ArbitraryMoreThan10()
        {
            var luckyNumbers = new[] { 1, 15, 100, 200, 300, 66, 400, 237, 1000, 3, 11, 87, 42, 723 };
            var dealersNumbers = Casino.FindBestDealerNumbers(luckyNumbers);

            Assert.Equal(0, Casino.CalculateTotalPriceMoney(luckyNumbers, dealersNumbers));
        }


    }
}
