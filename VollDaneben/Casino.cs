﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VollDaneben
{
    public static class Casino
    {
        private static readonly Random Random = new Random();

        public static int CalculateTotalPriceMoney(int[] luckyNumbers, int[] dealersNumbers) =>
            luckyNumbers.Select(l => l.MinimumDistanceToDealersNumber(dealersNumbers)).Sum();

        public static int MinimumDistanceToDealersNumber(this int luckyNumber, int[] dealersNumbers) =>
            dealersNumbers.Select(d => Math.Abs(luckyNumber - d)).Min();

        public static int FindClosestNumber(this int luckyNumber, int[] dealersNumbers) =>
            dealersNumbers
                .Select((d, idx) => (Math.Abs(luckyNumber - d), idx))
                .Aggregate((-1, -1), (curMin, x) => (curMin == (-1, -1) || x.Item1 < curMin.Item1) ? x : curMin)
                .Item2;

        public static int[] GenerateDealersNumbers(int length)
        {
            var randomNumbers = new HashSet<int>();
            while (randomNumbers.Count < length)
                randomNumbers.Add(Random.Next(1, 1000));
            return randomNumbers.ToArray();
        }

        public static int[] FindBestDealerNumbers(int[] luckyNumbers)
        {
            var dealerNumbers = GenerateDealersNumbers(10);
            for (var g = 0; g < 100000; g++)
            {
                dealerNumbers = luckyNumbers.OptimizeDealerNumbers(dealerNumbers);
            }

            return dealerNumbers;
        }

        private static int[] OptimizeDealerNumbers(this int[] luckyNumbers, int[] dealerNumbers)
        {
            var clusters = new List<int>[dealerNumbers.Length];
            for (var i = 0; i < clusters.Length; i++)
                clusters[i] = new List<int>();

            foreach (var luckyNumber in luckyNumbers)
            {
                var idx = luckyNumber.FindClosestNumber(dealerNumbers);
                clusters[idx].Add(luckyNumber);
            }

            var newDealerNumbers = new HashSet<int>();
            foreach (var cluster in clusters)
            {
                if (cluster.Count>0)
                    newDealerNumbers.Add((int) Math.Round(cluster.Average()));
            }

            while (newDealerNumbers.Count < dealerNumbers.Length)
                newDealerNumbers.Add(Random.Next(1, 1000));

            return newDealerNumbers.ToArray();

        }
    }
}
